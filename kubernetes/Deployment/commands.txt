#Create
kubectl create deployment kubernetes-bootcamp --image=gcr.io/google-samples/kubernetes-bootcamp:v1

#Read
kubectl get deployments
kubectl get deployment nginx-deployment
kubectl describe deployments


#Update
kubectl edit deployment/nginx-deployment
kubectl set image deployment/nginx-deployment nginx=nginx:1.16.1
kubectl set resources deployment/nginx-deployment -c=nginx --limits=cpu=200m,memory=512Mi

kubectl rollout pause deployment/nginx-deployment
kubectl rollout resume deployment/nginx-deployment

kubectl rollout status deployment/nginx-deployment
kubectl rollout history deployment/nginx-deployment

kubectl rollout undo deployment/nginx-deployment
kubectl rollout undo deployment/nginx-deployment --to-revision=2

kubectl scale deployment/nginx-deployment --replicas=10
kubectl autoscale deployment/nginx-deployment --min=10 --max=15 --cpu-percent=80

#Delete
kubectl delete deployment nginx-deployment

